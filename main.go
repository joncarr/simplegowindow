package main

import (
	"fmt"
	"runtime"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

func main() {
	runtime.LockOSThread()

	window := glfwInit()
	defer glfw.Terminate()

	if err := gl.Init(); err != nil {
		panic(fmt.Errorf("Problem intializing OpenGL: %v", err))
	}

	gl.ClearColor(0.0, 0.0, 0.0, 1.0)

	for !window.ShouldClose() {
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
		window.SwapBuffers()
		glfw.PollEvents()
	}

}

//Initializes and returns a Window with current OpenGL context
func glfwInit() *glfw.Window {

	if err := glfw.Init(); err != nil {
		panic(fmt.Errorf("Unable to initialize GLFW: %v", err))
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.Resizable, glfw.True)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	win, err := glfw.CreateWindow(800, 600, "Application Title", nil, nil)
	if err != nil {
		panic(fmt.Errorf("Unable to create GLFW window: %v", err))
	}

	win.MakeContextCurrent()

	return win
}
